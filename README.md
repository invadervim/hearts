# Hearts.Umbrella

This is a simple, single-player Hearts game built using Elixir and Phoenix LiveView, and Tailwind
CSS. It was created to showcase my coding style.

## Demo

A demo of this game can be [played on Heroku](https://elixir-hearts.herokuapp.com/). Rules of the
game can be found all over the internet, like [this site](https://bicyclecards.com/how-to-play/hearts/).
I've made this as simple as possible, with no extraneous rules, such as the Jack of Diamonds being
worth -10.

## Showcase

The app itself an umbrella app, split between the back-end (`app/hearts`) and the front-end
(`app/hearts_web`).

### Back-end

As this is a single-player game, I decided to forgo using a database. This keeps the game speedy,
without having to update the database on every card played, for example. Instead, a single game's
information is stored in [a single struct](https://gitlab.com/invadervim/hearts/-/tree/master/apps/hearts/lib/hearts/games/game.ex),
which includes a list of [rounds](https://gitlab.com/invadervim/hearts/-/tree/master/apps/hearts/lib/hearts/rounds/round.ex).

You'll find most of the meat inside the [`Games`](https://gitlab.com/invadervim/hearts/-/tree/master/apps/hearts/lib/hearts/games.ex)
and [`Rounds`](https://gitlab.com/invadervim/hearts/-/tree/master/apps/hearts/lib/hearts/rounds.ex)
contexts. `Games` is basically the gateway between the back- and front-end, as it was simpler to
call one context for what was needed and letting that context decide how to return the info.

Managing a `Game` struct can get messy, but that's where the
[`GameServer`](https://gitlab.com/invadervim/hearts/-/tree/master/apps/hearts/lib/hearts/game_server.ex)
comes in. The `GameServer` is a GenServer that handles any events from the front-end and will
update it's own instance of `Game` accordingly. The `GameServer` itself is supervised inside a
`Registry`.

As this is a single-player game, I needed an [`AI`](https://gitlab.com/invadervim/hearts/-/tree/master/apps/hearts/lib/hearts/ai.ex)
to play against. It's not much to look at, but it's there and can easily be updated with a much
better algorithm in the future.

### Front-end

[`GameLive`](https://gitlab.com/invadervim/hearts/-/tree/master/apps/hearts_web/lib/hearts_web/live/game_live.ex)
handles the entire game. It only calls `GameServer` directly whenever information about the game
is needed.

To help determine the AI's turn, a decision was made that the player was always `0`, so after
passing or playing a card, it could easily be checked if the `current_players_turn` was `0` or not.

I'm not a front-end developer (I'm sure it shows), but I did what I could with Tailwind CSS, which
I found relatively pleasant.

## Room for Improvement

Yes, definitely.

As far as playing the game, I probably won't make any improvements from here on out. However, if
you have constructive critism about the code itself, feel free to open an issue!

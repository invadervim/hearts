defmodule Hearts.Games.Game do
  @moduledoc """
  Represents multiple rounds of Hearts.
  """

  @enforce_keys [:id, :current_players_turn]
  defstruct id: nil, current_players_turn: nil, rounds: []

  @type t :: %__MODULE__{
          id: binary,
          current_players_turn: integer,
          rounds: list(Hearts.Rounds.Round.t())
        }

  @type ok_or_error :: :ok | {:error, String.t()}

  alias Hearts.{Cards, Games}

  @doc false
  @spec valid_pass_card?(t(), Games.player(), Cards.card()) :: ok_or_error
  def valid_pass_card?(%__MODULE__{} = game, player, card) do
    round = Games.current_round(game)

    with true <- validate_player_has_card(player, card, round),
         true <- validate_max_3_cards_selected(player, card, round) do
      :ok
    end
  end

  @doc false
  @spec valid_play_card?(t(), Games.player(), Cards.card()) :: ok_or_error
  def valid_play_card?(%__MODULE__{} = game, player, card) do
    round = Games.current_round(game)
    trick_cards = Games.get_trick_cards(game)

    with true <- validate_players_turn(game, player),
         true <- validate_player_has_card(player, card, round),
         true <- validate_starting_round_card(card, round),
         true <- validate_point_not_played_in_first_trick(card, round),
         true <- validate_matches_suit(game, card, round, trick_cards),
         true <- validate_hearts_broken(player, card, round, trick_cards) do
      :ok
    end
  end

  defp validate_max_3_cards_selected(player, card, %{cards: cards}) do
    round_card = Enum.find(cards, &(&1.held_by_player == player and &1.card == card))

    with false <- round_card.pass,
         false <- Enum.count(cards, &(&1.held_by_player == player and &1.pass)) < 3 do
      {:error, "only select 3 cards"}
    end
  end

  defp validate_players_turn(%{current_players_turn: current}, player) do
    with false <- current == player do
      {:error, "not player's turn"}
    end
  end

  defp validate_player_has_card(player, card, %{cards: cards}) do
    with false <- Enum.any?(cards, &(&1.held_by_player == player and &1.card == card)) do
      {:error, "card not found"}
    end
  end

  defp validate_starting_round_card(card, round) do
    with false <- !Hearts.Rounds.new?(round),
         false <- card == "2C" do
      {:error, "must start with 2C"}
    end
  end

  defp validate_point_not_played_in_first_trick(card, %{current_trick: current_trick}) do
    with false <- current_trick != 1,
         false <- Cards.point_value(card) < 1 do
      {:error, "cannot play point card on first trick"}
    end
  end

  defp validate_matches_suit(game, card, round, trick_cards) do
    with false <- new_trick?(trick_cards),
         starting_card <- get_starting_card(trick_cards, game.current_players_turn).card,
         false <- suits_match?(starting_card, card),
         false <- !player_has_matching_suit?(game, starting_card, round) do
      {:error, "card does not match starting suit"}
    end
  end

  defp get_starting_card(trick_cards, current_player) do
    starting_player = Games.previous_players_turn(trick_cards, current_player)
    Enum.find(trick_cards, &(&1.held_by_player == starting_player))
  end

  defp new_trick?(cards), do: rem(length(cards), 4) == 0

  defp suits_match?(<<_, starting_suit>>, <<_, playing_suit>>), do: starting_suit == playing_suit

  defp player_has_matching_suit?(game, <<_, starting_suit>>, %{cards: cards}) do
    Enum.any?(cards, fn round_card ->
      round_card.held_by_player == game.current_players_turn and
        round_card.trick == nil and
        Cards.suit(round_card.card) == <<starting_suit>>
    end)
  end

  defp validate_hearts_broken(player, card, round, trick_cards) do
    with false <- !new_trick?(trick_cards),
         false <- playing_non_heart?(card),
         false <- hearts_broken?(round),
         false <- !has_non_heart?(player, round) do
      {:error, "hearts has not been broken"}
    end
  end

  defp playing_non_heart?(card) do
    Cards.suit(card) != "H"
  end

  defp hearts_broken?(round) do
    Enum.any?(round.cards, &(&1.trick != nil and Cards.point_value(&1.card) > 0))
  end

  defp has_non_heart?(player, round) do
    Enum.any?(
      round.cards,
      &(&1.held_by_player == player and &1.trick == nil and Cards.suit(&1.card) != "H")
    )
  end
end

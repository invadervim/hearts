defmodule Hearts.Rounds.Round do
  @moduledoc """
  A round of Hearts (one deck).
  """

  @enforce_keys [:id, :cards, :passing]
  defstruct id: nil, cards: nil, current_trick: 1, passing: nil, scores: []

  @typedoc "All 52 cards in a round"
  @type deck :: list(Hearts.Rounds.RoundCard.t())

  @typedoc "A subset of deck"
  @type hand :: list(Hearts.Rounds.RoundCard.t())

  @typedoc "Determines direction to pass"
  @type direction :: :left | :right | :across | nil

  @type t :: %__MODULE__{
          id: integer,
          cards: deck,
          current_trick: integer,
          passing: direction,
          scores: list(integer)
        }

  @type ok_or_error :: :ok | {:error, String.t()}

  @doc false
  @spec validate_not_passing?(t) :: ok_or_error
  def validate_not_passing?(%__MODULE__{} = round) do
    if round.passing == nil, do: :ok, else: {:error, "select a card to play"}
  end

  @doc false
  @spec validate_passing?(t) :: ok_or_error
  def validate_passing?(%__MODULE__{} = round) do
    if round.passing != nil, do: :ok, else: {:error, "select a card to play"}
  end
end

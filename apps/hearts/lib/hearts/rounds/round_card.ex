defmodule Hearts.Rounds.RoundCard do
  @moduledoc """
  A card that has been dealt in the hand.
  """

  @enforce_keys [:card, :held_by_player]
  defstruct card: nil, held_by_player: nil, pass: false, taken_by_player: nil, trick: nil

  @type t :: %__MODULE__{
          card: Hearts.Cards.card(),
          held_by_player: Hearts.Games.player(),
          pass: boolean,
          taken_by_player: Hearts.Games.player(),
          trick: integer
        }
end

defmodule Hearts.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the PubSub system
      {Phoenix.PubSub, name: Hearts.PubSub},
      # Start a worker by calling: Hearts.Worker.start_link(arg)
      # {Hearts.Worker, arg}
      {Registry, keys: :unique, name: Hearts.GamesManager}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: Hearts.Supervisor)
  end
end

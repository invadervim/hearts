defmodule Hearts.GameServer do
  @moduledoc """
  Provides persistance to games.
  """

  use GenServer, restart: :transient

  alias Hearts.{AI, Games}

  def start_link() do
    {:ok, game} = Games.new()
    GenServer.start_link(__MODULE__, game, name: server_name(game.id))
    {:ok, game.id}
  end

  defp server_name(id), do: {:via, Registry, {Hearts.GamesManager, id}}

  def init(game), do: {:ok, game}

  defp call(id, instruction), do: GenServer.call(server_name(id), instruction)

  def clear_passed_cards(game_id), do: call(game_id, :clear_passed_cards)

  def current_players_turn(game_id), do: call(game_id, :current_players_turn)

  def current_round(game_id), do: call(game_id, :current_round)

  def game_finished?(game_id), do: call(game_id, :game_finished?)

  def new_round(game_id), do: call(game_id, :new_round)

  def pass_cards(game_id), do: call(game_id, :pass_cards)

  def passing?(game_id), do: call(game_id, :passing?)

  def pass_direction(game_id), do: call(game_id, :pass_direction)

  def play_card(game_id, player, card), do: call(game_id, {:play_card, player, card})

  def played_cards(game_id), do: call(game_id, :played_cards)

  def played_cards(game_id, trick), do: call(game_id, {:played_cards, trick})

  def players_hand(game_id, player), do: call(game_id, {:players_hand, player})

  def round_finished?(game_id), do: call(game_id, :round_finished?)

  def scores(game_id), do: call(game_id, :scores)

  def toggle_pass_card(game_id, player, card),
    do: call(game_id, {:toggle_pass_card, player, card})

  def suggest_card_to_play(game_id), do: call(game_id, :suggest_card_to_play)

  def suggest_cards_to_pass(game_id), do: call(game_id, :suggest_cards_to_pass)

  def total_scores(game_id), do: call(game_id, :total_scores)

  def trick_finished?(game_id), do: call(game_id, :trick_finished?)

  def users_hand(game_id), do: call(game_id, :users_hand)

  def handle_call(:clear_passed_cards, _, game) do
    game = Games.clear_passed_cards(game)
    {:reply, {:ok, game.id}, game}
  end

  def handle_call(:current_players_turn, _, game) do
    {:reply, game.current_players_turn, game}
  end

  def handle_call(:current_round, _, game) do
    {:reply, Games.current_round(game), game}
  end

  def handle_call(:game_finished?, _, game) do
    {:reply, Games.finished?(game), game}
  end

  def handle_call(:new_round, _, game) do
    game = Games.new_round(game)
    {:reply, {:ok, Games.current_round(game)}, game}
  end

  def handle_call(:pass_cards, _, game) do
    case Games.pass_cards(game) do
      {:ok, game} -> {:reply, {:ok, game.id}, game}
      {:error, game} -> {:reply, {:error, game}, game}
    end
  end

  def handle_call(:passing?, _, game) do
    round = Games.current_round(game)
    {:reply, round.passing != nil, game}
  end

  def handle_call(:pass_direction, _, game) do
    round = Games.current_round(game)
    {:reply, round.passing, game}
  end

  def handle_call({:play_card, player, card}, _, game) do
    case Games.play_card(game, player, card) do
      {:ok, game} -> {:reply, {:ok, game.id}, game}
      error -> {:reply, error, game}
    end
  end

  def handle_call(:played_cards, from, game) do
    round = Games.current_round(game)
    handle_call({:played_cards, round.current_trick}, from, game)
  end

  def handle_call({:played_cards, trick}, _, game) do
    cards =
      Games.get_trick_cards(game, trick)
      |> Enum.map(&{&1.held_by_player, &1.card})

    {:reply, cards, game}
  end

  def handle_call({:players_hand, player}, _, game) do
    hand = Games.current_hand(game, player)
    {:reply, hand, game}
  end

  def handle_call(:round_finished?, _, game) do
    {:reply, Games.round_finished?(game), game}
  end

  def handle_call(:scores, _, game) do
    {:reply, Games.scores(game), game}
  end

  def handle_call({:toggle_pass_card, player, card}, _, game) do
    case Games.toggle_pass_card(game, player, card) do
      {:ok, game} -> {:reply, {:ok, game.id}, game}
      error -> {:reply, error, game}
    end
  end

  def handle_call(:suggest_card_to_play, _, game) do
    {:reply, AI.suggest_card_to_play(game), game}
  end

  def handle_call(:suggest_cards_to_pass, _, game) do
    cards =
      Games.current_hand(game)
      |> Enum.map(& &1.card)

    {:reply, AI.suggest_cards_to_pass(cards), game}
  end

  def handle_call(:total_scores, _, game) do
    {:reply, Games.total_scores(game), game}
  end

  def handle_call(:trick_finished?, _, game) do
    {:reply, Games.trick_finished?(game), game}
  end

  def handle_call(:users_hand, from, game) do
    handle_call({:players_hand, 0}, from, game)
  end
end

defmodule Hearts.AI do
  @moduledoc """
  A painfully simple AI.
  """

  alias Hearts.{Cards, Games}
  alias Games.Game

  @doc """
  Returns the first playable card in the current player's hand.
  """
  @spec suggest_card_to_play(Game.t()) :: Cards.card()
  def suggest_card_to_play(%Game{} = game) do
    hand = Games.current_hand(game)
    find_playable_card(game, hand)
  end

  defp find_playable_card(game, [round_card | round_cards]) do
    case Game.valid_play_card?(game, game.current_players_turn, round_card.card) do
      :ok -> round_card.card
      {:error, _} -> find_playable_card(game, round_cards)
    end
  end

  @doc """
  Returns the 3 high value cards in the hand.
  """
  @spec suggest_cards_to_pass(list(Cards.card())) :: list(Cards.card())
  def suggest_cards_to_pass(cards) do
    cards
    |> Enum.sort(&(Cards.suit_value(&1) > Cards.suit_value(&2)))
    |> Enum.sort(&(Cards.rank_value(&1) < Cards.rank_value(&2)))
    |> Enum.sort(&(Cards.point_value(&1) > Cards.point_value(&2)))
    |> Enum.slice(0, 3)
  end
end

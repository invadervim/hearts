defmodule Hearts.Cards do
  @moduledoc """
  A 2-character representation (rank, suit) of a playing card.
  """

  @type card :: String.t()
  @type deck :: list(card)
  @type suit :: String.t()

  @ranks ~w|2 3 4 5 6 7 8 9 T J Q K A|
  @suits ~w|C D H S|

  @doc """
  Returns a shuffled, standard 52-card deck.
  """
  @spec deck() :: deck
  def deck, do: for(rank <- @ranks, suit <- @suits, do: rank <> suit) |> Enum.shuffle()

  @doc false
  @spec suit(card) :: suit
  def suit(<<_, suit>>), do: <<suit>>

  @doc """
  Sorts by rank and suit.
  """
  @spec sort(deck) :: deck
  def sort(cards) do
    cards
    |> Enum.sort(&(rank_value(&1) > rank_value(&2)))
    |> Enum.sort(&(suit_value(&1) < suit_value(&2)))
  end

  @doc "Used to sort by rank."
  @spec rank_value(card) :: integer
  def rank_value(<<rank, _>>), do: rank_value(<<rank>>)
  def rank_value("A"), do: 14
  def rank_value("K"), do: 13
  def rank_value("Q"), do: 12
  def rank_value("J"), do: 11
  def rank_value("T"), do: 10
  def rank_value(rank), do: String.to_integer(rank)

  @doc "Used to sort by suit."
  @spec suit_value(card) :: integer
  def suit_value(<<_, suit>>), do: suit_value(<<suit>>)
  def suit_value("C"), do: 1
  def suit_value("D"), do: 2
  def suit_value("S"), do: 3
  def suit_value("H"), do: 4

  @doc """
  Determines how many points a card is worth.
  """
  def point_value(<<rank, suit>>), do: point_value(<<rank>>, <<suit>>)
  def point_value(_, "H"), do: 1
  def point_value("Q", "S"), do: 13
  def point_value(_, _), do: 0

  @doc """
  Returns true if both suits are the same.
  """
  @spec matches_suit?(card, card) :: boolean
  def matches_suit?(<<_, primary_suit>>, <<_, other_suit>>), do: primary_suit == other_suit

  @doc """
  Returns true if the first card is a higher rank.
  """
  @spec higher_rank?(card, card) :: boolean
  def higher_rank?(<<primary_rank, _>>, <<other_rank, _>>) do
    rank_value(<<primary_rank>>) > rank_value(<<other_rank>>)
  end
end

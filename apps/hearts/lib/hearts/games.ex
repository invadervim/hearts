defmodule Hearts.Games do
  @moduledoc """
  Managing a game of Hearts.
  """

  @typedoc "The index of the player. The user is always 0."
  @type player :: integer

  alias Hearts.Games.Game
  alias Hearts.{Cards, Rounds}
  alias Rounds.Round

  @doc """
  Starts a new game by dealing the cards and setting the first player's turn.
  """
  @spec new() :: {:ok, Game.t()}
  def new() do
    {:ok, round} = Rounds.new(1)

    {:ok,
     %Game{
       id: Hearts.UUID.generate(),
       current_players_turn: Rounds.first_player_of_round(round),
       rounds: [round]
     }}
  end

  @doc """
  Starts a new round.
  """
  def new_round(%Game{} = game) do
    if round_finished?(game) do
      {:ok, round} = Rounds.new(length(game.rounds) + 1)

      game
      |> put_in([Access.key(:rounds)], [round | game.rounds])
      |> Map.put(:current_players_turn, Rounds.first_player_of_round(round))
    else
      game
    end
  end

  @doc """
  Selecting a card to pass.
  """
  @spec toggle_pass_card(Game.t(), player, list(Cards.card())) ::
          {:ok, Game.t()} | {:error, String.t()}
  def toggle_pass_card(%Game{} = game, player, card) do
    with :ok <- Round.validate_passing?(current_round(game)),
         :ok <- Game.valid_pass_card?(game, player, card) do
      {:ok, toggle_card_to_pass(game, player, card)}
    end
  end

  @doc """
  Passing after cards have been selected.
  """
  @spec pass_cards(Game.t()) :: {:ok | :error, Game.t()}
  def pass_cards(%Game{} = game) do
    round = current_round(game)

    if :ok == Round.validate_passing?(round) and Enum.count(round.cards, & &1.pass) == 12 do
      game =
        game
        |> update_in([Access.key(:rounds), Access.at(0)], fn round ->
          round
          |> Map.put(:cards, do_pass_cards(round.passing, round.cards))
          |> Map.put(:passing, nil)
        end)

      round = current_round(game)
      {:ok, Map.put(game, :current_players_turn, Rounds.first_player_of_round(round))}
    else
      {:error, Map.put(game, :current_players_turn, next_players_turn(game.current_players_turn))}
    end
  end

  defp do_pass_cards(direction, cards, updated_cards \\ [])

  defp do_pass_cards(_, [], updated_cards) do
    updated_cards
    |> Enum.sort(&(Cards.rank_value(&1.card) > Cards.rank_value(&2.card)))
    |> Enum.sort(&(Cards.suit_value(&1.card) < Cards.suit_value(&2.card)))
  end

  defp do_pass_cards(direction, [card | cards], updated_cards) do
    new_player =
      if card.pass do
        case direction do
          :left -> next_players_turn(card.held_by_player)
          :right -> previous_players_turn([1], card.held_by_player)
          :across -> next_players_turn(card.held_by_player) |> next_players_turn()
        end
      else
        card.held_by_player
      end

    do_pass_cards(direction, cards, [
      Map.merge(card, %{held_by_player: new_player}) | updated_cards
    ])
  end

  @doc """
  We want the pass cards to temporarily be shown after passing, then we can clear it with this.
  """
  @spec clear_passed_cards(Game.t()) :: Game.t()
  def clear_passed_cards(%Game{} = game) do
    update_in(game, [Access.key(:rounds), Access.at(0)], fn round ->
      Map.put(round, :cards, Enum.map(round.cards, &Map.put(&1, :pass, false)))
    end)
  end

  def previous_players_turn([], player), do: player
  def previous_players_turn([_ | trick_cards], 0), do: previous_players_turn(trick_cards, 3)

  def previous_players_turn([_ | trick_cards], player),
    do: previous_players_turn(trick_cards, player - 1)

  defp toggle_card_to_pass(game, player, card) do
    update_in(game, [Access.key(:rounds), Access.at(0), Access.key(:cards)], fn cards ->
      index = Enum.find_index(cards, &(&1.held_by_player == player and &1.card == card))
      round_card = Enum.at(cards, index)
      List.replace_at(cards, index, Map.put(round_card, :pass, !round_card.pass))
    end)
  end

  @doc """
  A player plays a card. Handles the validation.
  """
  @spec play_card(Game.t(), player, Cards.card()) :: {:ok, Game.t()} | {:error, String.t()}
  def play_card(%Game{} = game, player, card) do
    with :ok <- Round.validate_not_passing?(current_round(game)),
         :ok <- Game.valid_play_card?(game, player, card) do
      {:ok,
       game
       |> update_card_played(player, card)
       |> update_round()
       |> update_players_turn()}
    end
  end

  defp update_card_played(game, player, card) do
    update_in(game, [Access.key(:rounds), Access.at(0)], fn round ->
      Rounds.play_card(round, player, card)
    end)
  end

  defp update_round(game) do
    round = current_round(game)

    if Rounds.finished?(round) do
      update_in(game, [Access.key(:rounds), Access.at(0)], fn round ->
        Rounds.calculate_scores(round)
      end)
    else
      game
    end
  end

  defp update_players_turn(game) do
    Map.put(game, :current_players_turn, next_players_turn(game))
  end

  @doc """
  Grabs any played cards of the cufrent trick.
  """
  @spec get_trick_cards(Game.t(), integer | nil) :: Round.hand()
  def get_trick_cards(%Game{} = game, trick \\ nil) do
    if trick || (!round_finished?(game) and !trick_finished?(game)) do
      round = current_round(game)
      Enum.filter(round.cards, &(&1.trick == (trick || round.current_trick)))
    else
      []
    end
  end

  @doc false
  @spec current_round(Game.t()) :: Hearts.Rounds.Round.t()
  def current_round(%Game{rounds: [round | _]}), do: round

  @doc """
  Determines the next players turn by:
  * If it's a new round, the player with 2C.
  * If it's a new trick, the taker of the last trick.
  * The next player
  """
  @spec next_players_turn(Game.t() | integer) :: player
  def next_players_turn(%Game{} = game) do
    round = current_round(game)

    cond do
      Rounds.finished?(round) ->
        nil

      Rounds.trick_finished?(round) ->
        Enum.find(round.cards, &(&1.trick == round.current_trick - 1)).taken_by_player

      true ->
        next_players_turn(game.current_players_turn)
    end
  end

  def next_players_turn(player), do: rem(player + 1, 4)

  @doc """
  Return the current handle for a player.
  """
  @spec current_hand(Game.t()) :: Round.hand()
  @spec current_hand(Game.t(), player) :: Round.hand()
  def current_hand(%Game{} = game), do: current_hand(game, game.current_players_turn)

  def current_hand(%Game{} = game, player) do
    current_round(game)
    |> Map.get(:cards)
    |> Enum.filter(&(&1.held_by_player == player and &1.trick == nil))
  end

  @doc """
  Convenience method to call Rounds.finished?/1.
  """
  @spec round_finished?(Game.t()) :: boolean
  def round_finished?(%Game{} = game) do
    game
    |> current_round()
    |> Rounds.finished?()
  end

  @doc """
  Convenience method to call Rounds.trick_finished?/1.
  """
  @spec trick_finished?(Game.t()) :: boolean
  def trick_finished?(%Game{} = game) do
    game
    |> current_round()
    |> Rounds.trick_finished?()
  end

  @doc false
  @spec scores(Game.t()) :: list(list(integer))
  def scores(%Game{} = game) do
    # Don't want to show the current score
    if(round_finished?(game), do: game.rounds, else: List.delete_at(game.rounds, 0))
    |> Enum.map(& &1.scores)
    |> Enum.reverse()
  end

  @doc false
  @spec total_scores(Game.t()) :: list(integer)
  def total_scores(%Game{} = game) do
    Enum.reduce(scores(game), [0, 0, 0, 0], fn scores, total ->
      [
        Enum.at(scores, 0) + Enum.at(total, 0),
        Enum.at(scores, 1) + Enum.at(total, 1),
        Enum.at(scores, 2) + Enum.at(total, 2),
        Enum.at(scores, 3) + Enum.at(total, 3)
      ]
    end)
  end

  @doc false
  def finished?(game), do: Enum.any?(total_scores(game), &(&1 >= 100))
end

defmodule Hearts.Rounds do
  @moduledoc """
  Manages a round of Hearts.
  """

  alias Hearts.Rounds.{Round, RoundCard}
  alias Hearts.{Cards, Games}

  @doc """
  Starts a new round.
  """
  @spec new(integer) :: {:ok, Round.t()}
  def new(id) do
    {:ok, %Round{id: id, cards: new_round_cards(), current_trick: 1, passing: passing(id)}}
  end

  defp passing(id) do
    case rem(id, 4) do
      1 -> :left
      2 -> :right
      3 -> :across
      _ -> nil
    end
  end

  defp new_round_cards() do
    Cards.deck()
    |> Enum.chunk_every(13)
    |> Enum.with_index()
    |> Enum.map(fn {hand, index} ->
      hand
      |> Cards.sort()
      |> Enum.map(&%RoundCard{card: &1, held_by_player: index})
    end)
    |> List.flatten()
  end

  @doc """
  Returns true if no cards have been played.
  """
  @spec new?(Round.t()) :: boolean
  def new?(%Round{} = round), do: Enum.all?(round.cards, &(&1.trick == nil))

  @doc false
  def finished?(round), do: Enum.all?(round.cards, &(&1.trick != nil))

  @doc """
  Returns the player ID of whoever has the 2 of clubs.
  """
  @spec first_player_of_round(Round.t()) :: Games.player()
  def first_player_of_round(round) do
    round.cards
    |> Enum.find(&(&1.card == "2C"))
    |> Map.get(:held_by_player)
  end

  @doc """
  A player plays a card. Handles the actions of playing a card.
  """
  @spec play_card(Round.t(), Games.player(), Cards.card()) :: Round.t()
  def play_card(%Round{} = round, player, card) do
    round
    |> update_played_card(player, card)
    |> update_trick(player)
  end

  defp update_played_card(round, player, card) do
    index = Enum.find_index(round.cards, &(&1.held_by_player == player and &1.card == card))

    update_in(round, [Access.key(:cards), Access.at(index)], fn round_card ->
      Map.put(round_card, :trick, round.current_trick)
    end)
  end

  defp update_trick(round, player) do
    cards =
      round.cards
      |> Enum.filter(&(&1.trick == round.current_trick))
      |> Enum.sort(
        &(rem(&1.held_by_player + (3 - player), 4) <
            rem(&2.held_by_player + (3 - player), 4))
      )

    if length(cards) == 4 do
      trick_taker = get_trick_taker(cards)

      round
      |> update_taken_by_player(trick_taker, cards)
      |> Map.put(:current_trick, round.current_trick + 1)
    else
      round
    end
  end

  defp get_trick_taker([starting_card | round_cards]),
    do: get_trick_taker(round_cards, starting_card)

  defp get_trick_taker([], taker_card), do: taker_card.held_by_player

  defp get_trick_taker([player_card | round_cards], taker_card) do
    taker_card =
      with true <- Cards.matches_suit?(player_card.card, taker_card.card),
           true <- Cards.higher_rank?(player_card.card, taker_card.card) do
        player_card
      else
        _ -> taker_card
      end

    get_trick_taker(round_cards, taker_card)
  end

  defp update_taken_by_player(round, _, []), do: round

  defp update_taken_by_player(round, taken_by_player, [round_card | round_cards]) do
    index =
      Enum.find_index(round.cards, &(&1.trick == round_card.trick and &1.card == round_card.card))

    round =
      update_in(round, [Access.key(:cards), Access.at(index)], fn _ ->
        Map.put(round_card, :taken_by_player, taken_by_player)
      end)

    update_taken_by_player(round, taken_by_player, round_cards)
  end

  @doc false
  def trick_finished?(round) do
    round.current_trick != 1 and
      !Enum.any?(round.cards, &(&1.trick == round.current_trick))
  end

  @spec calculate_scores(Round.t()) :: Round.t()
  def calculate_scores(round) do
    scores =
      Enum.map([0, 1, 2, 3], fn player ->
        round.cards
        |> Enum.filter(&(&1.taken_by_player == player))
        |> Enum.map(&Cards.point_value(&1.card))
        |> Enum.sum()
      end)

    scores =
      if index = Enum.find_index(scores, &(&1 == 26)) do
        List.duplicate(26, 4)
        |> List.replace_at(index, 0)
      else
        scores
      end

    Map.put(round, :scores, scores)
  end
end

defmodule Hearts.CardsTest do
  use ExUnit.Case

  alias Hearts.Cards

  test "deck/0" do
    deck = Cards.deck()
    assert 52 = length(deck)
  end

  test "sort/1" do
    deck = Cards.sort(["AH" | Cards.deck()])
    assert ["2C", "3C", "4C", "5C", "6C", "7C", "8C", "9C", "TC" | _] = deck
    assert "AH" == List.last(deck)
  end

  test "point_value/1" do
    assert Cards.point_value("QC") == 0
    assert Cards.point_value("QH") == 1
    assert Cards.point_value("QS") == 13
  end
end

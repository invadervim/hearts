defmodule Hearts.RoundsTest do
  use ExUnit.Case

  alias Hearts.Rounds
  alias Rounds.Round

  test "new/1" do
    assert {:ok, %Round{}} = Rounds.new(1)
  end

  describe "calculate_scores/1" do
    test "successfully calculates" do
      {:ok, round} = Rounds.new(1)

      round =
        round.cards
        |> update_in(fn cards ->
          Enum.map(cards, fn round_card ->
            value = if Hearts.Cards.point_value(round_card.card) > 1, do: 0, else: 1
            Map.put(round_card, :taken_by_player, value)
          end)
        end)
        |> Rounds.calculate_scores()

      assert [13, 13, 0, 0] = round.scores
    end

    test "shoots the moon" do
      {:ok, round} = Rounds.new(1)

      round =
        round.cards
        |> update_in(fn cards -> Enum.map(cards, &Map.put(&1, :taken_by_player, 0)) end)
        |> Rounds.calculate_scores()

      assert [0, 26, 26, 26] = round.scores
    end
  end
end

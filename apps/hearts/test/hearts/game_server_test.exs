defmodule Hearts.GameServerTest do
  use ExUnit.Case

  alias Hearts.GameServer, as: Server
  alias Hearts.Rounds.Round

  setup do
    {:ok, game_id} = Server.start_link()
    {:ok, passed_game_id} = Server.start_link()
    pass_cards(passed_game_id)

    %{
      id: game_id,
      player: Server.current_players_turn(game_id),
      passed_id: passed_game_id,
      passed_player: Server.current_players_turn(passed_game_id)
    }
  end

  test "clear_passed_cards/1", %{id: id} do
    player = Server.current_players_turn(id)
    hand = Server.players_hand(id, player)
    Server.toggle_pass_card(id, player, hd(hand).card)

    hand = Server.players_hand(id, player)
    assert Enum.any?(hand, & &1.pass)

    Server.clear_passed_cards(id)
    hand = Server.players_hand(id, player)
    refute Enum.any?(hand, & &1.pass)
  end

  test "current_players_turn/1 and players_hand/2", %{id: id, player: player} do
    hand = Server.players_hand(id, player)
    assert "2C" == hd(hand).card
  end

  test "current_round/1", %{id: id} do
    assert %Round{id: 1} = Server.current_round(id)
  end

  describe "new_round/1" do
    test "starts a new round", %{passed_id: id} do
      Enum.each(1..52, fn _ ->
        player = Server.current_players_turn(id)
        suggested_card = Server.suggest_card_to_play(id)
        assert {:ok, _} = Server.play_card(id, player, suggested_card)
      end)

      {:ok, %Round{id: 2}} = Server.new_round(id)
    end

    test "does not start a new round because current one is not finished", %{id: id} do
      {:ok, %Round{id: 1}} = Server.new_round(id)
    end
  end

  test "pass_cards/1", %{id: id} do
    assert {:error, _} = Server.pass_cards(id)
  end

  test "pass_direction/1", %{id: id} do
    assert :left = Server.pass_direction(id)
  end

  describe "play_card/3" do
    test "successfully plays a card", %{passed_id: id, passed_player: player} do
      assert {:ok, _} = Server.play_card(id, player, "2C")
    end

    test "doesn't play a card", %{id: id, player: player} do
      assert {:error, _} = Server.play_card(id, player, "3C")
    end
  end

  test "played_cards/1", %{passed_id: id, passed_player: player} do
    assert [] = Server.played_cards(id)

    {:ok, _} = Server.play_card(id, player, "2C")
    assert [{player, "2C"}] == Server.played_cards(id)
  end

  test "played_cards/2", %{passed_id: id, passed_player: player} do
    {:ok, _} = Server.play_card(id, player, "2C")
    assert [{player, "2C"}] == Server.played_cards(id, 1)
  end

  test "round_finished?/1", %{passed_id: id} do
    refute Server.round_finished?(id)

    Enum.each(1..52, fn _ ->
      player = Server.current_players_turn(id)
      suggested_card = Server.suggest_card_to_play(id)
      assert {:ok, _} = Server.play_card(id, player, suggested_card)
    end)

    assert Server.round_finished?(id)
  end

  test "scores/1", %{passed_id: id} do
    assert [] == Server.scores(id)

    Enum.each(1..52, fn _ ->
      player = Server.current_players_turn(id)
      suggested_card = Server.suggest_card_to_play(id)
      assert {:ok, _} = Server.play_card(id, player, suggested_card)
    end)

    [scores] = Server.scores(id)
    assert Enum.sum(scores) == 26 or Enum.sum(scores) == 78
  end

  test "suggest_card_to_play/1", %{id: id} do
    assert "2C" == Server.suggest_card_to_play(id)
  end

  test "total_scores/1", %{id: id} do
    assert [0, 0, 0, 0] == Server.total_scores(id)
  end

  test "trick_finished?/1", %{passed_id: id, passed_player: player} do
    refute Server.trick_finished?(id)

    {:ok, _} = Server.play_card(id, player, "2C")

    Enum.each(1..3, fn _ ->
      player = Server.current_players_turn(id)
      hand = Server.players_hand(id, player)
      {:ok, _} = Server.play_card(id, player, hd(hand).card)
    end)

    assert Server.trick_finished?(id)
  end

  test "users_hand/1", %{id: id} do
    round = Server.current_round(id)
    card = Enum.find(round.cards, &(&1.held_by_player == 0))
    assert card in Server.users_hand(id)
  end

  def pass_cards(id, index \\ 4)
  def pass_cards(_, index) when index == 0, do: nil

  def pass_cards(id, index) do
    Server.suggest_cards_to_pass(id)
    |> Enum.each(fn card -> Server.toggle_pass_card(id, Server.current_players_turn(id), card) end)

    Server.pass_cards(id)

    pass_cards(id, index - 1)
  end
end

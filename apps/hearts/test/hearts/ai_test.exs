defmodule Hearts.AITest do
  use ExUnit.Case

  test "suggest_card_to_play/1" do
    {:ok, game} = Hearts.Games.new()
    assert "2C" = Hearts.AI.suggest_card_to_play(game)
  end

  test "suggest_cards_to_pass/1" do
    assert ["QS", "AH", "AS"] = Hearts.AI.suggest_cards_to_pass(~w|AH KS QS AD AC AS|)
  end
end

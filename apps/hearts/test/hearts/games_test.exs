defmodule Hearts.GamesTest do
  use ExUnit.Case

  alias Hearts.Games
  alias Games.Game

  test "new/0" do
    assert {:ok, %Game{}} = Games.new()
  end

  describe "toggle_pass_card/3" do
    setup do
      {:ok, game} = Games.new()
      {:ok, passed_game} = Games.new()
      %{game: game, passed_game: pass_cards(passed_game)}
    end

    test "toggles a card", %{game: game} do
      cards = Games.current_hand(game, game.current_players_turn)
      refute hd(cards).pass

      assert {:ok, %Game{} = game} =
               Games.toggle_pass_card(game, game.current_players_turn, hd(cards).card)

      cards = Games.current_hand(game, game.current_players_turn)
      assert hd(cards).pass

      {:ok, %Game{} = game} =
        Games.toggle_pass_card(game, game.current_players_turn, hd(cards).card)

      cards = Games.current_hand(game, game.current_players_turn)
      refute hd(cards).pass
    end

    test "does not toggle because card is not in player's hand", %{game: game} do
      round = Games.current_round(game)
      cards = round.cards -- Games.current_hand(game, game.current_players_turn)

      {:error, "card not found"} =
        Games.toggle_pass_card(game, game.current_players_turn, hd(cards).card)
    end

    test "does not toggle because max reached", %{game: game} do
      player = game.current_players_turn
      cards = Games.current_hand(game, game.current_players_turn)

      {:ok, game} = Games.toggle_pass_card(game, player, Enum.at(cards, 0).card)
      {:ok, game} = Games.toggle_pass_card(game, player, Enum.at(cards, 1).card)
      {:ok, game} = Games.toggle_pass_card(game, player, Enum.at(cards, 2).card)

      {:error, "only select 3 cards"} =
        Games.toggle_pass_card(game, player, Enum.at(cards, 3).card)
    end

    test "does not toggle because no longer passing", %{passed_game: game} do
      player = game.current_players_turn
      cards = Games.current_hand(game, game.current_players_turn)
      {:error, "select a card to play"} = Games.toggle_pass_card(game, player, hd(cards).card)
    end
  end

  describe "play_card/3" do
    setup do
      {:ok, game} = Games.new()
      %{game: pass_cards(game)}
    end

    test "successfully plays a card", %{game: game} do
      card = "2C"
      current_player = game.current_players_turn
      refute get_round_card(game, card).trick

      assert {:ok, game} = Games.play_card(game, game.current_players_turn, card)
      assert get_round_card(game, card).trick == 1
      assert game.current_players_turn == next_player(current_player)

      card = (players_hand(game) |> hd()).card
      assert {:ok, game} = Games.play_card(game, game.current_players_turn, card)

      card = (players_hand(game) |> hd()).card
      assert {:ok, game} = Games.play_card(game, game.current_players_turn, card)

      card = (players_hand(game) |> hd()).card
      assert {:ok, game} = Games.play_card(game, game.current_players_turn, card)

      round = Games.current_round(game)
      taken_by_player = Enum.find(round.cards, &(&1.trick == 1)).taken_by_player
      assert round.current_trick == 2
      assert game.current_players_turn == taken_by_player
    end

    test "successfully plays last card of round", %{game: game} do
      round = Games.current_round(game)

      h2 = Enum.find(round.cards, &(&1.card == "2H"))
      h2_index = Enum.find_index(round.cards, &(&1.card == "2H"))

      h3 = Enum.find(round.cards, &(&1.card == "3H"))
      h3_index = Enum.find_index(round.cards, &(&1.card == "3H"))

      h4 = Enum.find(round.cards, &(&1.card == "4H"))
      h4_index = Enum.find_index(round.cards, &(&1.card == "4H"))

      ah = Enum.find(round.cards, &(&1.card == "AH"))
      ah_index = Enum.find_index(round.cards, &(&1.card == "AH"))

      point_player_4 = ah.held_by_player
      point_player_22 = Games.next_players_turn(point_player_4)
      next_player_1 = Games.next_players_turn(point_player_22)
      next_player_2 = Games.next_players_turn(next_player_1)

      game =
        game
        |> update_in([Access.key(:rounds), Access.at(0), Access.key(:cards)], fn cards ->
          cards
          |> Enum.map(&Map.put(&1, :taken_by_player, point_player_22))
          |> Enum.map(&Map.put(&1, :trick, 1))
          |> List.replace_at(ah_index, Map.merge(ah, %{taken_by_player: nil, trick: nil}))
          |> List.replace_at(
            h2_index,
            Map.merge(h2, %{held_by_player: next_player_1, taken_by_player: nil, trick: 13})
          )
          |> List.replace_at(
            h3_index,
            Map.merge(h3, %{held_by_player: next_player_2, taken_by_player: nil, trick: 13})
          )
          |> List.replace_at(
            h4_index,
            Map.merge(h4, %{
              held_by_player: point_player_22,
              taken_by_player: nil,
              trick: 13
            })
          )
        end)
        |> update_in([Access.key(:rounds), Access.at(0), Access.key(:current_trick)], fn _ ->
          13
        end)
        |> Map.put(:current_players_turn, point_player_4)

      assert {:ok, game} = Games.play_card(game, point_player_4, "AH")

      round = Games.current_round(game)
      assert Enum.at(round.scores, point_player_4) == 4
      assert Enum.at(round.scores, point_player_22) == 22

      # Easiest to test these here, as well
      assert [round.scores] == Games.scores(game)

      assert length(game.rounds) == 1
      game = Games.new_round(game)
      assert length(game.rounds) == 2
    end

    test "does not play card because it's passing time" do
      {:ok, game} = Games.new()
      assert {:error, _} = Games.play_card(game, game.current_players_turn, "2C")
    end

    test "does not play card because it's not their turn", %{game: game} do
      next_player = next_player(game.current_players_turn)
      assert {:error, "not player's turn"} = Games.play_card(game, next_player, "2C")
    end

    test "does not play card because they don't have the card", %{game: game} do
      {:ok, game} = Games.play_card(game, game.current_players_turn, "2C")
      assert {:error, "card not found"} = Games.play_card(game, game.current_players_turn, "2C")
    end

    test "does not play card because it's not 2C", %{game: game} do
      non_2c_card =
        game
        |> players_hand()
        |> List.last()
        |> Map.get(:card)

      assert {:error, "must start with 2C"} =
               Games.play_card(game, game.current_players_turn, non_2c_card)
    end

    test "does not play card because it has a positive point value", %{game: game} do
      assert {:ok, game} = Games.play_card(game, game.current_players_turn, "2C")
      game = replace_players_hand(game, ["AH"])

      assert {:error, "cannot play point card on first trick"} =
               Games.play_card(game, game.current_players_turn, "AH")
    end

    test "does not play card because it does not match the starting suit", %{game: game} do
      assert {:ok, game} = Games.play_card(game, game.current_players_turn, "2C")

      game = replace_players_hand(game, ~w|AC AD|)

      assert {:error, "card does not match starting suit"} =
               Games.play_card(game, game.current_players_turn, "AD")

      game = replace_players_hand(game, ~w|AD|)
      assert {:ok, game} = Games.play_card(game, game.current_players_turn, "AD")

      game = replace_players_hand(game, ~w|KC KD|)

      assert {:error, "card does not match starting suit"} =
               Games.play_card(game, game.current_players_turn, "KD")

      assert {:ok, _game} = Games.play_card(game, game.current_players_turn, "KC")
    end

    test "does not play card because hearts has not been broken", %{game: game} do
      assert {:ok, game} = Games.play_card(game, game.current_players_turn, "2C")

      card = (players_hand(game) |> hd()).card
      assert {:ok, game} = Games.play_card(game, game.current_players_turn, card)

      card = (players_hand(game) |> hd()).card
      assert {:ok, game} = Games.play_card(game, game.current_players_turn, card)

      card = (players_hand(game) |> hd()).card
      assert {:ok, game} = Games.play_card(game, game.current_players_turn, card)

      game = replace_players_hand(game, ~w|AD AH|)

      assert {:error, "hearts has not been broken"} =
               Games.play_card(game, game.current_players_turn, "AH")

      game = replace_players_hand(game, ~w|AH|)
      assert {:ok, _} = Games.play_card(game, game.current_players_turn, "AH")
    end
  end

  defp next_player(3), do: 0
  defp next_player(player), do: player + 1

  defp get_round_card(game, card) do
    game
    |> Games.current_round()
    |> Map.get(:cards)
    |> Enum.find(&(&1.card == card))
  end

  defp players_hand(game) do
    game
    |> Games.current_round()
    |> Map.get(:cards)
    |> Enum.filter(&(&1.held_by_player == game.current_players_turn))
  end

  defp replace_players_hand(game, new_cards) do
    player = game.current_players_turn

    update_in(game, [Access.key(:rounds), Access.at(0), Access.key(:cards)], fn round_cards ->
      Enum.reject(round_cards, &(&1.held_by_player == player)) ++
        Enum.map(new_cards, &%Hearts.Rounds.RoundCard{card: &1, held_by_player: player})
    end)
  end

  def pass_cards(game, index \\ 4)
  def pass_cards(game, index) when index == 0, do: game

  def pass_cards(game, index) do
    {_, game} =
      Games.current_hand(game)
      |> Enum.map(& &1.card)
      |> Hearts.AI.suggest_cards_to_pass()
      |> toggle_card(game)
      |> Games.pass_cards()

    pass_cards(game, index - 1)
  end

  def toggle_card([], game), do: game

  def toggle_card([card | cards], game) do
    {_, game} = Games.toggle_pass_card(game, game.current_players_turn, card)
    toggle_card(cards, game)
  end
end

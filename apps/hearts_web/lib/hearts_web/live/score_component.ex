defmodule HeartsWeb.ScoreComponent do
  @moduledoc false

  use HeartsWeb, :live_component

  @impl true
  def update(assigns, socket) do
    {:ok, assign(socket, assigns)}
  end

  @impl true
  def render(assigns) do
    ~L"""
    <table class="table-auto">
      <thead>
        <tr class="rounded-lg font-medium" style="background-color: #ddd">
          <th class="p-3">Round</th>
          <th class="p-3">Player 0 (You)</th>
          <th class="p-3">Player 1</th>
          <th class="p-3">Player 2</th>
          <th class="p-3">Player 3</th>
        </tr>
      </thead>

      <tbody>
        <%= for {scores, round} <- @scores do %>
          <tr>
            <td class="p-3 border-b text-lg text-center"><%= round + 1 %></td>

            <%= for score <- scores do %>
              <td class="p-3 border-b text-lg text-center"><%= score %></td>
            <% end %>
          </tr>
        <% end %>
      </tbody>

      <tfoot>
        <tr class="font-medium">
          <td class="p-3">Total</th>

          <%= for {total, player} <- Enum.with_index(@totals) do %>
            <td class="p-3 text-lg text-center<%= winner(@game_ended?, @totals, player) %>"><%= total %></td>
          <% end %>
        </tr>
      </tfoot>
    </table>
    """
  end

  def winner(false, _, _), do: ""

  def winner(_, scores, player) do
    if player in winners(scores), do: " font-bold bg-green-100", else: ""
  end

  defp winners(scores, index \\ 0, low_score \\ 100, winners \\ [])

  defp winners([], _, _, winners), do: winners

  defp winners([score | scores], index, low_score, winners) do
    {new_score, new_winners} =
      cond do
        score < low_score -> {score, [index]}
        score == low_score -> {score, [index | winners]}
        true -> {low_score, winners}
      end

    winners(scores, index + 1, new_score, new_winners)
  end
end

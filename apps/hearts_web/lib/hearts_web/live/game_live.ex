defmodule HeartsWeb.GameLive do
  @moduledoc false

  use HeartsWeb, :live_view

  require Integer

  alias Hearts.GameServer, as: Server

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :new, _params) do
    {:ok, game_id} = Server.start_link()

    socket
    |> assign(:page_title, "Play Hearts")
    |> push_patch(to: Routes.game_path(socket, :play, game_id))
  end

  defp apply_action(socket, :play, %{"id" => game_id}) do
    auto_play(game_id)

    socket
    |> assign(defaults(game_id))
    |> assign(:page_title, "Play Hearts")
    |> assign(:pass_direction, Server.pass_direction(game_id))
    |> direct_user()
  end

  defp apply_action(socket, :scores, %{"id" => game_id}) do
    socket
    |> assign(defaults(game_id))
    |> assign(:page_title, "Scores")
    |> assign(:component, HeartsWeb.ScoreComponent)
    |> assign(:scores, Server.scores(game_id) |> Enum.with_index())
    |> assign(:totals, Server.total_scores(game_id))
    |> direct_user()
  end

  defp defaults(game_id) do
    %{
      game_id: game_id,
      game_ended?: Server.game_finished?(game_id),
      played_cards: Server.played_cards(game_id),
      round_ended?: Server.round_finished?(game_id),
      users_hand: Server.users_hand(game_id)
    }
  end

  defp auto_play(game_id) do
    if Server.current_players_turn(game_id) != 0 do
      cond do
        Server.passing?(game_id) ->
          player = Server.current_players_turn(game_id)

          Server.suggest_cards_to_pass(game_id)
          |> Enum.each(fn card -> Server.toggle_pass_card(game_id, player, card) end)

          with {:ok, _} <- Server.pass_cards(game_id) do
            Process.send_after(self(), :clear_passed_cards, 2000)
          end

          auto_play(game_id)

        !Server.round_finished?(game_id) ->
          time_to_wait = if Server.trick_finished?(game_id), do: 1000, else: 200
          Process.send_after(self(), :auto_play_card, time_to_wait)

        true ->
          nil
      end
    end
  end

  @impl true
  def handle_info(:auto_play_card, %{assigns: %{game_id: game_id}} = socket) do
    player = Server.current_players_turn(game_id)
    suggested_card = Server.suggest_card_to_play(game_id)
    handle_event("play", %{"player" => "#{player}", "card" => suggested_card}, socket)
  end

  @impl true
  def handle_info(:clear_passed_cards, %{assigns: %{game_id: game_id}} = socket) do
    Server.clear_passed_cards(game_id)
    {:noreply, assign(socket, :users_hand, Server.users_hand(game_id))}
  end

  @impl true
  def handle_info(:clear_trick, socket) do
    {:noreply, assign(socket, :played_cards, Server.played_cards(socket.assigns.game_id))}
  end

  @impl true
  def handle_info(:round_ended, %{assigns: %{game_id: game_id}} = socket) do
    {:noreply,
     socket
     |> assign(:round_ended?, Server.round_finished?(game_id))
     |> assign(:game_ended?, Server.game_finished?(game_id))
     |> push_patch(to: Routes.game_path(socket, :scores, game_id))}
  end

  @impl true
  def handle_event("new_round", _, %{assigns: %{game_id: game_id}} = socket) do
    Server.new_round(game_id)
    auto_play(game_id)

    {:noreply,
     socket
     |> assign(:users_hand, Server.users_hand(game_id))
     |> assign(:pass_direction, Server.pass_direction(game_id))
     |> assign(:round_ended?, false)
     |> direct_user()}
  end

  @impl true
  def handle_event("select_pass", %{"player" => player, "card" => card}, socket) do
    player = String.to_integer(player)

    case Server.toggle_pass_card(socket.assigns.game_id, player, card) do
      {:ok, _} ->
        {:noreply, assign(socket, :users_hand, Server.users_hand(socket.assigns.game_id))}

      {:error, error_msg} ->
        {:noreply, put_flash(socket, :error, error_msg)}
    end
  end

  @impl true
  def handle_event("pass_cards", _, %{assigns: %{game_id: game_id}} = socket) do
    with {:ok, _} <- Server.pass_cards(game_id) do
      Process.send_after(self(), :clear_passed_cards, 2000)
    end

    auto_play(game_id)

    {:noreply,
     socket
     |> assign(:users_hand, Server.users_hand(game_id))
     |> assign(:pass_direction, Server.pass_direction(game_id))
     |> direct_user()}
  end

  @impl true
  def handle_event("play", %{"player" => player, "card" => card}, socket) do
    game_id = socket.assigns.game_id
    player = String.to_integer(player)

    case Server.play_card(game_id, player, card) do
      {:ok, _} ->
        round = Server.current_round(game_id)

        last_played_card_trick =
          if Server.trick_finished?(game_id),
            do: round.current_trick - 1,
            else: round.current_trick

        socket =
          socket
          |> clear_flash()
          |> finish_trick(game_id)
          |> assign(:users_hand, Server.users_hand(game_id))
          |> assign(:played_cards, Server.played_cards(game_id, last_played_card_trick))

        auto_play(game_id)

        if Server.round_finished?(game_id), do: Process.send_after(self(), :round_ended, 1000)

        {:noreply, socket}

      {:error, error_msg} ->
        {:noreply, put_flash(socket, :error, error_msg)}
    end
  end

  defp finish_trick(socket, game_id) do
    if Server.trick_finished?(game_id) do
      Process.send_after(self(), :clear_trick, 1000)

      round = Server.current_round(game_id)
      card = Enum.find(round.cards, &(&1.trick == round.current_trick - 1))
      put_flash(socket, :info, "Player #{card.taken_by_player} takes the trick")
    else
      socket
    end
  end

  defp show_played_card(played_cards, player) do
    with {_, card} <- List.keyfind(played_cards, player, 0) do
      card_img(card, rotate: Integer.is_odd(player))
    end
  end

  defp selected_3_cards?(hand), do: Enum.count(hand, & &1.pass) == 3

  defp direct_user(%{assigns: %{game_id: game_id}} = socket) do
    dir = Server.pass_direction(game_id)

    if Server.current_players_turn(game_id) && dir do
      msg = if dir == :across, do: dir, else: "to the #{dir}"
      put_flash(socket, :help, "Pass 3 cards #{msg}")
    else
      socket
    end
  end
end

defmodule HeartsWeb.LiveHelpers do
  @moduledoc false

  import Phoenix.LiveView.Helpers

  @doc """
  Renders a component inside the `HeartsWeb.ModalComponent` component.

  The rendered modal receives a `:return_to` option to properly update
  the URL when the modal is closed.

  ## Examples

      <%= live_modal @socket, HeartsWeb.ScoreLive.FormComponent,
        id: @score.id || :new,
        action: @live_action,
        score: @score,
        return_to: Routes.score_index_path(@socket, :index) %>
  """
  def live_modal(socket, component, opts) do
    path = Keyword.fetch!(opts, :return_to)

    modal_opts = [
      id: :modal,
      title: opts[:page_title],
      return_to: path,
      component: component,
      opts: opts
    ]

    live_component(socket, HeartsWeb.ModalComponent, modal_opts)
  end
end

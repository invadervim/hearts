defmodule HeartsWeb.HomeLive do
  @moduledoc false

  use HeartsWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, :page_title, "Hearts")}
  end

  @impl true
  def render(assigns) do
    ~L"""
    <div class="h-screen w-full flex flex-col justify-center items-center">
      <div class='max-w-lg bg-white shadow-md rounded-lg overflow-hidden mx-auto'>
        <div class="py-4 px-8 mt-3">
          <div class="flex flex-col mb-8">
            <h2 class="text-gray-700 font-semibold text-2xl tracking-wide mb-2">Hearts</h2>
            <p class="text-gray-500 text-base">A simple demo of the classic trick-taking game. Learn more at</p>
            <a href="https://gitlab.com/invadervim/hearts" class="text-gray-500 text-base">https://gitlab.com/invadervim/hearts</p>
          </div>

          <div class="items-center">
            <%= live_redirect "Play!", to: Routes.game_path(@socket, :new), class: "block tracking-widest uppercase text-center shadow bg-indigo-600 hover:bg-indigo-700 focus:shadow-outline focus:outline-none text-white text-xs py-3 px-10 rounded" %>
          </div>
        </div>
      </div>
    </div>
    """
  end
end

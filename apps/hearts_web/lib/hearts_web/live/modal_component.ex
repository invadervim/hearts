defmodule HeartsWeb.ModalComponent do
  @moduledoc false

  use HeartsWeb, :live_component

  @impl true
  def render(assigns) do
    ~L"""
    <div id="<%= @id %>" class="phx-modal h-screen w-full flex justify-center items-center"
      phx-capture-click="close"
      phx-window-keydown="close"
      phx-key="escape"
      phx-target="#<%= @id %>"
      phx-page-loading>

      <div class="absolute fixed bg-white rounded-lg">
        <div class="flex flex-col items-start p-4">
          <div class="flex items-center w-full">
            <div class="font-medium text-lg">
              <%= @title %>
            </div>

            <div class="ml-auto fill-current text-gray-700 cursor-pointer">
              <%= live_patch raw("&times;"), to: @return_to, class: "phx-modal-close" %>
            </div>
          </div>
        </div>

        <%= live_component @socket, @component, @opts %>
      </div>
    </div>
    """
  end

  @impl true
  def handle_event("close", _, socket) do
    {:noreply, push_patch(socket, to: socket.assigns.return_to)}
  end
end

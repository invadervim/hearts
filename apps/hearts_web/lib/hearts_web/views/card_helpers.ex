defmodule HeartsWeb.CardHelpers do
  @moduledoc """
  Display a card from an SVG.
  """

  use Phoenix.HTML

  @card_width -5.38
  @card_height -7.82

  def card_img(card, opts \\ []) do
    translate = if opts[:pass], do: " -translate-y-6", else: ""
    rotate = if opts[:rotate], do: " rotate-90", else: ""

    ~E"""
    <div class="card transform<%= translate %><%= rotate %>" style="background-position: <%= column(card) %>rem <%= row(card) %>rem"></div>
    """
  end

  def column(<<rank, _>>) do
    column =
      case <<rank>> do
        "A" -> 0
        "K" -> 12
        "Q" -> 11
        "J" -> 10
        "T" -> 9
        rank -> String.to_integer(rank) - 1
      end

    column * @card_width
  end

  def row(<<_, suit>>), do: Enum.find_index(~w|C D H S|, &(&1 == <<suit>>)) * @card_height

  def card_path(socket, card) do
    HeartsWeb.Router.Helpers.static_path(socket, "/images/cards.svg##{card_name(card)}")
  end

  defp card_name(<<rank, suit>>), do: "#{rank_name(<<rank>>)}_#{suit_name(<<suit>>)}"

  defp rank_name(rank) do
    case rank do
      "A" -> "1"
      "K" -> "king"
      "Q" -> "queen"
      "J" -> "jack"
      "T" -> "10"
      _ -> rank
    end
  end

  defp suit_name(suit) do
    case suit do
      "C" -> "club"
      "D" -> "diamond"
      "H" -> "heart"
      "S" -> "spade"
    end
  end
end

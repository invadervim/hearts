defmodule HeartsWeb.GameLiveTest do
  use HeartsWeb.ConnCase

  import Phoenix.LiveViewTest

  test "new game", %{conn: conn} do
    {:ok, _game_live, html} =
      live(conn, Routes.game_path(conn, :new))
      |> follow_redirect(conn)

    assert html =~ "span phx-click=\"select_pass\""
  end

  test "view scores", %{conn: conn} do
    {:ok, game_live, html} =
      live(conn, Routes.game_path(conn, :new))
      |> follow_redirect(conn)

    refute html =~ "Player 0 (You)"

    html =
      game_live
      |> element("a", "View Scores")
      |> render_click()

    assert html =~ "Player 0 (You)"
  end

  test "select cards to pass", %{conn: conn} do
    {:ok, game_live, html} =
      live(conn, Routes.game_path(conn, :new))
      |> follow_redirect(conn)

    refute html =~ "-translate-y-6"

    hand = players_hand(html)

    assert select_card(game_live, hd(hand)) =~ "-translate-y-6"
    refute select_card(game_live, hd(hand)) =~ "-translate-y-6"
  end

  describe "pass cards" do
    test "successfully passes cards", %{conn: conn} do
      {:ok, game_live, html} =
        live(conn, Routes.game_path(conn, :new))
        |> follow_redirect(conn)

      assert html =~ "select_pass"

      hand = players_hand(html)
      Enum.each(0..2, &select_card(game_live, Enum.at(hand, &1)))
      assert render(game_live) =~ "-translate-y-6"

      render_hook(game_live, "pass_cards")
      html = render(game_live)
      refute html =~ "select_pass"
    end

    test "doesn't pass cards", %{conn: conn} do
      {:ok, game_live, html} =
        live(conn, Routes.game_path(conn, :new))
        |> follow_redirect(conn)

      assert html =~ "select_pass"

      render_hook(game_live, "pass_cards")
      assert html =~ "select_pass"
    end
  end

  describe "play cards" do
    test "successfully plays card", %{conn: conn} do
      {:ok, game_live, html} =
        live(conn, Routes.game_path(conn, :new))
        |> follow_redirect(conn)

      refute html =~ "phx-click=\"play"

      hand = players_hand(html)
      Enum.each(0..2, &select_card(game_live, Enum.at(hand, &1)))
      render_hook(game_live, "pass_cards")

      # Let computer play
      :timer.sleep(800)
      html = render(game_live)
      assert html =~ "phx-click=\"play"

      assert html
             |> players_hand()
             |> length() == 13

      hand = players_hand(html)

      render_hook(game_live, "play", %{"player" => "0", "card" => hd(hand)})

      assert game_live
             |> render()
             |> players_hand()
             |> length() == 12
    end

    test "does not play a card during passing phase", %{conn: conn} do
      {:ok, game_live, html} =
        live(conn, Routes.game_path(conn, :new))
        |> follow_redirect(conn)

      refute html =~ "select a card to play"

      hand = players_hand(html)
      render_hook(game_live, "play", %{"player" => "0", "card" => hd(hand)})
      assert render(game_live) =~ "select a card to play"
    end
  end

  defp players_hand(html) do
    Regex.scan(~r|phx-value-card="(.*?)"|, html)
    |> Enum.map(&Enum.at(&1, 1))
  end

  defp select_card(live, card) do
    render_hook(live, "select_pass", %{"player" => "0", "card" => card})
  end
end

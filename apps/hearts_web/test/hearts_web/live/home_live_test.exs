defmodule HeartsWeb.HomeLiveTest do
  use HeartsWeb.ConnCase

  import Phoenix.LiveViewTest

  test "visit home", %{conn: conn} do
    {:ok, home_live, html} = live(conn, "/")
    assert html =~ "invadervim"

    {:ok, _home_live, html} =
      home_live
      |> element("a", "Play")
      |> render_click()
      |> follow_redirect(conn, Routes.game_path(conn, :new))
      |> follow_redirect(conn)

    assert html =~ "span phx-click=\"select_pass\""
  end
end
